function NombreEnConsola(){
for (i = 0; i < 100; i++){
    console.log("Pablo Mendez Suarez");
}
}

function verExperiencia() {
var element = document.getElementById('experience');

if (element.classList.contains("active") == false){
    element.classList.add("active");
    document.getElementById('studies').classList.remove("active");
    document.getElementById('abilities').classList.remove("active");
}
else{
    document.getElementById('experience').classList.remove("active");
}
}

function verEstudios() {
var element = document.getElementById('studies');

if (element.classList.contains("active") == false){
    element.classList.add("active");
    document.getElementById('experience').classList.remove("active");
    document.getElementById('abilities').classList.remove("active");
}
else{
    document.getElementById('studies').classList.remove("active");
}
}

function verHabilidades(){
var element = document.getElementById('abilities');

if (element.classList.contains("active") == false){
    element.classList.add("active");
    document.getElementById('experience').classList.remove("active");
    document.getElementById('studies').classList.remove("active");
}
else{
    document.getElementById('abilities').classList.remove("active");
}
}


function verIdiomas() {
var element = document.getElementById('languages');

if (element.classList.contains("active") == false){
    element.classList.add("active");
}
else{
    document.getElementById('languages').classList.remove("active");
}
}

function verTecnicas() {
var element = document.getElementById('techniques');

if (element.classList.contains("active") == false){
    element.classList.add("active");
}
else{
    document.getElementById('techniques').classList.remove("active");
}
}

function verPersonales() {
var element = document.getElementById('personal');

if (element.classList.contains("active") == false){
    element.classList.add("active");
}
else{
    document.getElementById('personal').classList.remove("active");
}
}

